export default function Pokedex(props:{title: string, imageUrl: string,description: string}){
    return(
        <div className="Poke">
            <h1>{props.title}</h1>
            <img src={props.imageUrl} alt="" />
            <h3>{props.description}</h3>
        </div>
    )
}