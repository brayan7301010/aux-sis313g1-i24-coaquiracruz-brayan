export default function VideoPlayer(props:{title: string, author: string,video: string}){
    return(
        <div className="video-com">
            <h1>{props.title}</h1>
            <h3>{props.author}</h3>
            <video src={props.video}></video>
        </div>
    )
}