import Image from "next/image";
import styles from "./page.module.css";
import Pokedex from "@/componets/Pokemon";
import VideoPlayer from "@/componets/Video";
export default function Home() {
  return (
    <div className="cajon">
      <div className="father">
        <div className="list-pokemon">
          <Pokedex title={"CHARIZARTD"} imageUrl={"./megacharti.webp"} description={"Fuego"}></Pokedex>
        </div>
        <div className="list-pokemon2">
         <Pokedex title={"GENGAR"} imageUrl={"/megagengar.jpg"} description={"Fantasma yVeneno."}></Pokedex>
        </div>
        <div className="list-pokemon3">
          <Pokedex title={"TYRANITAR"} imageUrl={"./tyranitar.jpg"} description={"Roca y Siniestro"}></Pokedex>
        </div>
      </div>
      <div className="mother">
        <div className="uno">
        <VideoPlayer title={"Bajo la mesa"} author={"Morat"} video={"./Bajo la mesa - Martín Vargas (Morat).mp4"}></VideoPlayer>
        </div>
        <div className="dos">
        <VideoPlayer title={"You Broke Me Firts"} author={"conor Maynard"} video={"./Conor Maynard - You Broke Me First (Sub Español & English) __ Male Versión - Dirty Secrets.mp4"}></VideoPlayer>
        </div>
        <div className="tree">
        <VideoPlayer title={"Egoista"} author={"Ozuna"} video={"./Ozuna -  Egoísta feat. Zion y Lennox (Video Oficial).mp4"}></VideoPlayer>
        </div>
      </div>
    </div>
  );
}
