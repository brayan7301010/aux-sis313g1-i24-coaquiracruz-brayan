# ***PRÁCTICA N°7***

***Univ.: Brayan Coaquira Cruz***

***Docente: Ing. Jose David Mamani Figueroa***

***Auxiliar: Univ. Sergio Moises Apaza Caballero***

***Materia: DISEÑO Y PROGRAMACIÓN GRÁFICA***

***Siglas: SIS-313 G1***

***Fecha: 23/04/2024***

## ***AQUI ESTA EL ENLACE DEL VIDEO***

https://terabox.com/s/1b7_DLceM4J4ng3KyYRwSvg

![](/practica7/portadas/Captura%20de%20pantalla%202024-04-23%20201456.png)

## ***CUALES SON LOS COMADOS QUE SE USO PARA LA REALIZACION DEL VIDEO?***
 - [x] ***WINDOWS+R***
 - [x] ***cmd***
 - [x] ***cd ..***
 - [x] ***mkdir "nombre de la carpeta"***
 - [x] ***cd***

## ***CUALES SERIAN 5 COMANDOS UTILIZADOS EN LA TERMINAL Y SU FORMA DE USARLOS EN WINDOWS Y LINUX***
***COMANDSO PARA WINDOWS***
 - [x] ***cd=(Cambiar directorio): Este comando te permite moverte entre directorios.***
 - [x] ***dir= (Listar contenido): Muestra el contenido del directorio actual, incluyendo subcarpetas y archivos. ***
 - [x] ***tree Carpeta= (Mostrar árbol de directorios): Genera un árbol visual de la estructura de carpetas dentro de la carpeta especificada.***
 - [x] ***cls=(Limpiar pantalla): Borra todo el texto en la ventana de la consola, dejándola limpia y lista para nuevos comandos.***
 - [x] ***exit=(Cerrar CMD): Cierra la ventana del Símbolo del Sistema.***
***COMANDSO PARA LINUX***
 - [x] ***ls=(Listar archivos y directorios): Muestra todos los archivos y carpetas en el directorio actual.***
 - [x] ***pwd= (Mostrar ruta actual): Imprime la ruta completa del directorio actual.***
 - [x] ***cd=(Cambiar directorio): Al igual que en Windows, puedes usar cd para cambiar de directorio en Linux.***
 - [x] ***mkdir= (Crear directorio): Crea una nueva carpeta en el directorio actual. Por ejemplo, mkdir NuevaCarpeta creará una carpeta llamada “NuevaCarpeta”.***
 - [x] ***cp=(Copiar archivos): Copia archivos de un lugar a otro. Por ejemplo, cp archivo.txt /ruta/destino copiará “archivo.txt” al destino especificado***
