# ***PRÁCTICA N°5***

***Univ.: Brayan Coaquira Cruz***

***Docente: Ing. Jose David Mamani Figueroa***

***Auxiliar: Univ. Sergio Moises Apaza Caballero***

***Materia: DISEÑO Y PROGRAMACIÓN GRÁFICA***

***Siglas: SIS-313 G1***

***Fecha: 2/04/2024***

## ***SIN MAS REPAROS EMPEZAMOS***
![INICIOS](/practica5/portadas/portadaxD.png)

- [x] ***COMPLETAR LOS PRIMEROS 5 NIVELES Y LOS ULTIMOS 10, MOSTRANDO UNA CAPTURA COMPLETA DE LA PANTALLA CON EL NIVEL VENCIDO***

     - [x] ***Primeros 5 nivles***

#  **NIVEL 1**
![nivel1](/practica5/portadas/nivel1.png)   ![portada](/practica5/portadas/EXITOS.png)
## ***Solucion:***
### ***plate***
    Next level --->

#  **NIVEL 2**
![nicel2](/practica5/portadas/nivel2.png)   ![portada](/practica5/portadas/EXITOS.png)
## ***Solucion:***
### ***bento***
    Next level --->

#  **NIVEL 3**
![nivel3](/practica5/portadas/nivel3.png)   ![portada](/practica5/portadas/EXITOS.png)
## ***Solucion:***
### ***#fancy***
    Next level --->

#  **NIVEL 4**
![nivel4](/practica5/portadas/nivel4.png)   ![portada](/practica5/portadas/EXITOS.png)
## ***Solucion:***
### ***plate apple***
    Next level --->

#  **NIVEL 5**
![nivel5](/practica5/portadas/nivel5.png)   ![portada](/practica5/portadas/EXITOS.png)
## ***Solucion:***
### ***#fancy pickle***
    Next level --->

   - [x] ***Ultimos 10 nivles***

#  **NIVEL 22**
![](/practica5/portadas/nivel22.png)  ![portada](/practica5/portadas/EXITOS.png)
## ***Solucion:***
### ***plate:nth-of-type(2n+3)***
    Next level --->

#  **NIVEL 23**
![](/practica5/portadas/nivel23.png)   ![portada](/practica5/portadas/EXITOS.png)
## ***Solucion:***
### ***plate apple:only-of-type***
    Next level --->

#  **NIVEL 24**
![](/practica5/portadas/nivel24.png)   ![portada](/practica5/portadas/EXITOS.png)
## ***Solucion:***
### ***orange:last-of-type, apple:last-of-type***
    Next level --->

#  **NIVEL 25**
![](/practica5/portadas/25.png)   ![portada](/practica5/portadas/EXITOS.png)
### ***bento:empty***
    Next level --->

#  **NIVEL 26**
![](/practica5/portadas/26.png)   ![portada](/practica5/portadas/EXITOS.png)
## ***Solucion:***
### ***apple:not(.small)***
    Next level --->

#  **NIVEL 27**
![](/practica5/portadas/27.png)   ![portada](/practica5/portadas/EXITOS.png)
## ***Solucion:***
### ***[for]***
    Next level --->

#  **NIVEL 28**
![](/practica5/portadas/28.png)   ![portada](/practica5/portadas/EXITOS.png)
## ***Solucion:***
### ***plate[for]***
    Next level --->

#  **NIVEL 29**
![](/practica5/portadas/29.png)   ![portada](/practica5/portadas/EXITOS.png)
## ***Solucion:***
### ***[for="Vitaly"]***
    Next level --->

#  **NIVEL 30**
![](/practica5/portadas/30.png)   ![portada](/practica5/portadas/EXITOS.png)
## ***Solucion:***
### ***[for^="Sa"]***
    Next level --->

#  **NIVEL 31**
![](/practica5/portadas/31.png)   ![portada](/practica5/portadas/EXITOS.png)
## ***Solucion:***
### ***[for$="ato"]***
    Next level --->

#  **NIVEL 32**
![](/practica5/portadas/32.png)   ![portada](/practica5/portadas/EXITOS.png)
## ***Solucion:***
### ***[for*="obb"]***
    End Level --->