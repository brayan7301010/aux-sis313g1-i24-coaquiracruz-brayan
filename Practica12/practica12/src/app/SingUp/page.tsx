
import Button from "@/componentes/Button"
import CajaDeContras from "@/componentes/CajaDeContras"
import CajaDeRegistro from "@/componentes/CajaDeRegistro"
import Logo from "@/componentes/Logo"
import Publiciti from "@/componentes/Publiciti"
import Remenber from "@/componentes/Remenber"
import Titulo from "@/componentes/Titulo"
import Link from "next/link"

const SingUp=() => {
    return(
        <div className="cajita1">
            <div className="cajita3">
                <img src="/imagenes/lateral2.svg" alt="" />
            </div>
            <div>
                <div className="cajita3">
                    <Logo image={"/imagenes/logo.svg"} title={"Your Logo"}></Logo>
                    <Titulo title={"Login"} paragraph={"Login to access your travelwise  account"}></Titulo>
                </div>
                <div className="cajasdelogin1">
                    <CajaDeRegistro tile={"Email"}></CajaDeRegistro>
                    <CajaDeRegistro tile={"Email"}></CajaDeRegistro>
                </div>
                <div className="cajasdelogin2">
                    <CajaDeRegistro tile={"Email"}></CajaDeRegistro>
                    <CajaDeRegistro tile={"Email"}></CajaDeRegistro>
                </div>
                <div className="cajasdelogin3">
                    <CajaDeContras tile={"Password"}></CajaDeContras>
                    <CajaDeContras tile={"Confirm Password"}></CajaDeContras>
                </div>
                <p>Don’t have an account?</p><Link href="/" className='active'>Login</Link>
                <div>
                    <Remenber title={"I agree to all the Terms and Privacy Policies"} img={"/imagenes/cubito.svg"} ></Remenber>
                </div>
                    <Button title={"Create account"}></Button>
                <div className="pu">
                    <Publiciti img={"/imagenes/face.svg"}></Publiciti>
                    <Publiciti img={"/imagenes/google.svg"}></Publiciti>
                    <Publiciti img={"/imagenes/aple.svg"}></Publiciti>
                </div>  
            </div>
            
        </div>
    )
}
export default SingUp;