'use client';

import Image from "next/image";
import styles from "./page.module.css";
import Logo from "@/componentes/Logo";
import Titulo from "@/componentes/Titulo";
import CajaDeRegistro from "@/componentes/CajaDeRegistro";
import CajaDeContras from "@/componentes/CajaDeContras";
import Remenber from "@/componentes/Remenber";
import Button from "@/componentes/Button";
import { useRef } from "react";
import Link from "next/link";
import Publiciti from "@/componentes/Publiciti";

export default function Home() {
  const myref:any=useRef(null);
  return (
    <div className="cajota">
      <div className="cajaizquierda">
        <div>
          <Logo image={"/imagenes/logo.svg"} title={"Your Logo"}></Logo>
          <Titulo title={"Login"} paragraph={"Login to access your travelwise  account"}></Titulo>
        </div>
        <div>
          <CajaDeRegistro tile={"Email"}></CajaDeRegistro>
          <CajaDeContras tile={"Password"}></CajaDeContras>
          <Remenber title={"Remember me"} paragraph={"Forgot Password"} img={"/imagenes/cubito.svg"}></Remenber>
        </div>
        <Button title={"Login"}></Button> 
        <div className="ao">
          <p>Don’t have an account?</p><Link href="/SingUp" className='active'>Sign up</Link>
        </div> 
        
        <div className="pu">
          <Publiciti img={"/imagenes/face.svg"}></Publiciti>
          <Publiciti img={"/imagenes/google.svg"}></Publiciti>
          <Publiciti img={"/imagenes/aple.svg"}></Publiciti>
        </div>
      </div>
      <div className="cajaderecha">
        <img src="/imagenes/lateral.svg" alt="" />
      </div>
    </div>
  )
}
