interface LogoProps  {
    image:string;
    title:string;
}
const Logo = ({image, title} :LogoProps)=>{
    return (
        <div className="logo">
            <img src={image} alt="" />
            <h4>{title}</h4>
        </div>
    )
}
export default Logo;