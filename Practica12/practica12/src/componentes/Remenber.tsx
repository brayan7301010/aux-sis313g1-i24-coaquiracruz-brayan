interface RemenberProps {
    title:string;
    paragraph?:string;
    img:string;
}
const Remenber = ({title, paragraph, img} :RemenberProps)=>{
    return (
        <div className="remen">
            <img src={img} alt="" />
            <h4>{title}</h4>
            {paragraph?<p>{paragraph}</p>:" "}
        </div>
    )
}
export default Remenber;