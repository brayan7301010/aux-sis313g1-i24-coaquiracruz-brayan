interface PublicitiProps {
    img:string;
}
const Publiciti=({img}: PublicitiProps)=> {
    return(
        <div className="public">
            <img src={img} alt="" />
        </div>
    )
}
export default Publiciti;