interface TituloProps {
    title:string;
    paragraph:string;
}
const Titulo = ({title, paragraph} :TituloProps)=>{
    return (
        <div className="titulo">
            <h1>{title}</h1>
            <p>{paragraph}</p>
        </div>
    )
}
export default Titulo;