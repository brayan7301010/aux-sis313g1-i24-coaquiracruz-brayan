interface ButtonProps{
    title: string;
}
const Button =({title}: ButtonProps)=> {
    return(
        <div className="but">
            <h2>{title}</h2>
        </div>
    )
}
export default Button;