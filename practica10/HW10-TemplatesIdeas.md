# ***PRÁCTICA N°8***

***Univ.: Brayan Coaquira Cruz***

***Docente: Ing. Jose David Mamani Figueroa***

***Auxiliar: Univ. Sergio Moises Apaza Caballero***

***Materia: DISEÑO Y PROGRAMACIÓN GRÁFICA***

***Siglas: SIS-313 G1***

***Fecha: 06/05/2024***

## ***Enlaces de los templates***
1. https://www.figma.com/file/yE0y8mvAtEo87iqn6IvPaM/Virtual-Headset-Landing-Page-UI-FREEBIE-(Community)?type=design&node-id=0-1&mode=design&t=qjlNyfMZuSoUr74q-0

2. https://www.figma.com/file/Jzs13Exq07s68a5cPg9PR3/Gym-Fitnees-Hompage-Design-Template-(Community)?type=design&node-id=0-1&mode=design&t=5ULxubse0WCi48GQ-0


## ***Enlace del repositorio***
https://gitlab.com/brayan7301010/react-with-brayancoaquiracruz.git