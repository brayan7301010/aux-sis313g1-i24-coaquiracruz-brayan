# ***PRÁCTICA N°8***

***Univ.: Brayan Coaquira Cruz***

***Docente: Ing. Jose David Mamani Figueroa***

***Auxiliar: Univ. Sergio Moises Apaza Caballero***

***Materia: DISEÑO Y PROGRAMACIÓN GRÁFICA***

***Siglas: SIS-313 G1***

***Fecha: 23/04/2024***

 - [x] ***MUESTRE EN UNA CAPTURA DE PATALLA LAS VERSIONES DE NOBE Y NPM***
![](/practica8/portadas/Captura%20de%20pantalla%202024-04-23%20204958.png)

## ***AQUI ESTA EL ENLACE DEL VIDEO***
https://terabox.com/s/1Pbj4H6zeww25NHDokFpfHw

## ***CUALES SON LOS COMADOS QUE SE USO PARA LA REALIZACION DEL VIDEO?***
 - [x] ***WINDOWS+R***
 - [x] ***cmd***
 - [x] ***cd ..***
 - [x] ***mkdir "nombre de la carpeta"***
 - [x] ***cd***

## ***CUALES SON LOS ERRORES QUE TUVO PARA LA REALIZACION DEL VIDEO?***
 - [x] ***LA NO EXISTENCIA DE UNA CARPETA QEU CONTENDRA EL PROYECTO***
![](/practica8/portadas/WhatsApp%20Image%202024-04-23%20at%2021.18.35.jpeg)

## ***CUALES EL EL PROBLEMA Y LA SOLUCION?***
 - [x] ***EL PROBLEMA SE TRATA ES POR QUE DENTRO DEL PROYECTO HAY UNA CARPERTA (.git) indicando que dentro del mismo hay otro proyecto***
  - [x] ***La solucion trata en eliminar el la carperta dentro del proyecto qeu tengas la terminacion(.git)***